package com.example.shoppingcart;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

/**
 * UI Class for managing a store
 * @author David Anderson
 */
public class ManageStoreActivity extends Activity
{
	private DataBaseHelper myDbHelper;

	private Store myStore;
	private TextView storeLabel;

	private String[] storeInfo;	
	private String storeName;
	private String storeLocation;


	/**
	 * Method executes on screen startup
	 */
	protected void onCreate(Bundle savedInstanceState) 
	{
		super.onCreate(savedInstanceState);	   
		setContentView(R.layout.manage_store);

		storeLabel = (TextView) findViewById(R.id.manageStoreTextView);			
		Intent myIntent = getIntent();		
		myStore = new Store();
		storeInfo = (String[]) myIntent.getSerializableExtra("key");

		storeName = storeInfo[0];
		storeLocation = storeInfo[1];				

		storeLabel.setText("Manage " + storeName + "\n" + "(" + storeLocation + ")" );
	}


	/**
	 * Method handles manage items button
	 * @param v view
	 */
	public void manageAddItems(View v)
	{
		Intent myIntent = new Intent(ManageStoreActivity.this, ManageStoreItemActivity.class);	
		myIntent.putExtra("key", storeInfo);
		startActivity(myIntent);
		finish(); 		//finished here to handle the error for "threat exiting with uncaught exception"
	}


	/**
	 * Method handles deleting a store
	 * @param v view
	 */
	public void deleteStore(View v)
	{
		AlertDialog.Builder alert = new AlertDialog.Builder(ManageStoreActivity.this);
		String storeLoc = storeLabel.getText().toString().trim();
		final String storeLocFixed = storeLoc.replace("Manage", "");
		alert.setTitle("Are you sure you want to delete store " + storeLocFixed + " ?");						

		alert.setPositiveButton("Yes", new DialogInterface.OnClickListener()
		{
			public void onClick(DialogInterface dialog, int whichButton) 
			{					
				myDbHelper = new DataBaseHelper(ManageStoreActivity.this);				
				myStore.deleteStore(myDbHelper, storeName, storeLocation);

				Toast.makeText(getApplicationContext(), storeLocFixed + " deleted from your list of stores", Toast.LENGTH_SHORT).show();	

				startActivity(new Intent(ManageStoreActivity.this, MainActivity.class));  
			}								
		});																				

		alert.setNegativeButton("Cancel", new DialogInterface.OnClickListener() 
		{
			public void onClick(DialogInterface dialog, int whichButton) {}
		});

		alert.show();	
	}


	/**
	 * Method handles changing a store name
	 * @param v view
	 */
	public void renameStore(View v)
	{
		// Pop-up for confirming rename
		AlertDialog.Builder alert = new AlertDialog.Builder(ManageStoreActivity.this);
		alert.setTitle("Enter new store name");						
		final EditText storeNameInput = new EditText(ManageStoreActivity.this);
		alert.setView(storeNameInput);

		alert.setPositiveButton("Rename", new DialogInterface.OnClickListener()
		{						
			public void onClick(DialogInterface dialog, int whichButton) 
			{								
				String renamedStore = storeNameInput.getText().toString().trim();												
				myDbHelper = new DataBaseHelper(ManageStoreActivity.this);	

				if (renamedStore.contains("'"))
				{
					new AlertDialog.Builder(ManageStoreActivity.this)
					.setTitle("Error")
					.setMessage("No single quotes allowed")
					.setPositiveButton("Ok", new DialogInterface.OnClickListener() 
					{
						public void onClick(DialogInterface dialog, int which) {}
					})	
					.show();
				}

				// Check for duplicate stores
				else if (myStore.hasDuplicateStores(myDbHelper, renamedStore, storeLocation) == true)
				{
					new AlertDialog.Builder(ManageStoreActivity.this)
					.setTitle("Error")
					.setMessage("The store you entered already exists. Please enter a new store name or store location.")
					.setPositiveButton("Ok", new DialogInterface.OnClickListener() 
					{
						public void onClick(DialogInterface dialog, int which) {}
					})	
					.show();
				}									

				else
				{
					myStore.renameStore(myDbHelper, renamedStore, storeName, storeLocation);			    
					Toast.makeText(getApplicationContext(), storeName + " renamed to " + renamedStore, Toast.LENGTH_SHORT).show();														
					storeLabel.setText("Manage " + renamedStore + "\n" + "(" + storeLocation + ")" );
				}
			}								
		});			

		alert.setNegativeButton("Cancel", new DialogInterface.OnClickListener() 
		{
			public void onClick(DialogInterface dialog, int whichButton){}
		});

		alert.show();	
	}


	/**
	 * Method handles changing store location
	 * @param v view
	 */
	public void changeLocation(View v)
	{
		// Pop-up for confirming rename
		AlertDialog.Builder alert = new AlertDialog.Builder(ManageStoreActivity.this);
		alert.setTitle("Enter new store location");						
		final EditText storeNameInput = new EditText(ManageStoreActivity.this);
		alert.setView(storeNameInput);

		alert.setPositiveButton("Rename", new DialogInterface.OnClickListener()
		{						
			public void onClick(DialogInterface dialog, int whichButton) 
			{								
				String newStoreLocation = storeNameInput.getText().toString().trim();							
				myDbHelper = new DataBaseHelper(ManageStoreActivity.this);	

				if (newStoreLocation.contains("'"))
				{
					new AlertDialog.Builder(ManageStoreActivity.this)
					.setTitle("Error")
					.setMessage("No single quotes allowed")
					.setPositiveButton("Ok", new DialogInterface.OnClickListener() 
					{
						public void onClick(DialogInterface dialog, int which) {}
					})	
					.show();
				}

				// Check for duplicate stores
				else if (myStore.hasDuplicateStores(myDbHelper, storeName, newStoreLocation) == true)
				{
					new AlertDialog.Builder(ManageStoreActivity.this)
					.setTitle("Error")
					.setMessage("The store you entered already exists. Please enter a new store name or store location.")
					.setPositiveButton("Ok", new DialogInterface.OnClickListener() 
					{
						public void onClick(DialogInterface dialog, int which) {}
					})	
					.show();
				}	

				else
				{
					myStore.renameLocation(myDbHelper, storeName, newStoreLocation, storeLocation);
					Toast.makeText(getApplicationContext(), "Location " + storeLocation + " changed to " + newStoreLocation, Toast.LENGTH_SHORT).show();														
					storeLabel.setText("Manage " + storeName + "\n" + "(" + newStoreLocation + ")" );
				}
			}								
		});			

		alert.setNegativeButton("Cancel", new DialogInterface.OnClickListener() 
		{
			public void onClick(DialogInterface dialog, int whichButton){}
		});

		alert.show();	
	}


	/**
	 * Method handles finding the location of user specified item
	 * @param v view
	 */
	public void findItemLocation(View v)
	{
		AlertDialog.Builder alert = new AlertDialog.Builder(ManageStoreActivity.this);
		alert.setTitle("Find");		
		alert.setMessage("Enter item name you would like to find the location for");
		final EditText itemNameInput = new EditText(ManageStoreActivity.this);
		alert.setView(itemNameInput);

		alert.setPositiveButton("Find", new DialogInterface.OnClickListener()
		{						
			public void onClick(DialogInterface dialog, int whichButton) 
			{								
				String itemName = itemNameInput.getText().toString().trim();	

				if (itemName.contains("'"))
				{
					new AlertDialog.Builder(ManageStoreActivity.this)
					.setTitle("Error")
					.setMessage("No single quotes allowed")
					.setPositiveButton("Ok", new DialogInterface.OnClickListener() 
					{
						public void onClick(DialogInterface dialog, int which) {}
					})	
					.show();
				}

				else
				{
					myDbHelper = new DataBaseHelper(ManageStoreActivity.this);	

					String itemLocation = myStore.findStoreItemLocation(myDbHelper, storeName, storeLocation, itemName);

					if (itemLocation.equals("Not Found"))
					{
						new AlertDialog.Builder(ManageStoreActivity.this)
						.setTitle("Error")
						.setMessage("Item could not be found")
						.setPositiveButton("Ok", new DialogInterface.OnClickListener() 
						{
							public void onClick(DialogInterface dialog, int which) {}
						})	
						.show();
					}

					else
					{
						new AlertDialog.Builder(ManageStoreActivity.this)
						.setTitle("Item Location Found")
						.setMessage(itemName + " is located at: " + itemLocation)
						.setPositiveButton("Ok", new DialogInterface.OnClickListener() 
						{
							public void onClick(DialogInterface dialog, int which) {}
						})	
						.show();				
					}
				}
			}
		});	

		alert.setNegativeButton("Cancel", new DialogInterface.OnClickListener() 
		{
			public void onClick(DialogInterface dialog, int whichButton){}
		});

		alert.show();	
	}
}