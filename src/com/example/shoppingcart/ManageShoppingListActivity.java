package com.example.shoppingcart;

import java.io.IOException;
import java.util.ArrayList;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.util.SparseBooleanArray;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.Toast;
import android.widget.AdapterView.OnItemSelectedListener;

/**
 * UI Class for managing shopping lists
 * 
 * @author David Anderson, Marin Rangelov, Bertram Bishop
 * 
 */
public class ManageShoppingListActivity extends Activity {
	private DataBaseHelper myDbHelper;
	private ShoppingList myShoppingList;
	private SortList mySortList;
	private Store myStore;
	private String storeName;
	private String storeLocation;
	private ListView itemListView;
	private ArrayAdapter<String> adapter;
	private SQLiteDatabase database;
	private ArrayList<String> itemList;
	private String itemNames;
	private SparseBooleanArray checkedPositions;
	private String selectedItems;

	protected void onCreate(Bundle savedInstanceState) 
	{
		super.onCreate(savedInstanceState);
		setContentView(R.layout.manage_shopping_list);

		myShoppingList = new ShoppingList();
		mySortList = new SortList();
		populateStoreSpinner();
		populateItemNames();

		itemListView = (ListView) findViewById(R.id.shoppingItemsListView);
		createItemList();
		adapter = new ArrayAdapter<String>(this,
				android.R.layout.simple_list_item_multiple_choice, itemList);
		itemListView.setChoiceMode(ListView.CHOICE_MODE_MULTIPLE);
		itemListView.setAdapter(adapter);
		adapter.notifyDataSetChanged();
	}


	/**
	 * Method refreshes store list
	 */
	public void onResume()
	{
		super.onResume();		
		populateStoreSpinner();
	}


	/**
	 * Clear all selected checked items making them unchecked
	 */
	private void ClearSelections() 
	{
		int count = this.itemListView.getAdapter().getCount();

		for (int i = 0; i < count; i++) 
			this.itemListView.setItemChecked(i, false);		
	}


	/**
	 * Retrieve the data from the database to be displayed
	 * in the list view
	 */
	public void createItemList() 
	{
		myDbHelper = new DataBaseHelper(this);
		database = myDbHelper.getWritableDatabase();
		myDbHelper.openDataBase();
		Cursor cursor = database.rawQuery(
				"SELECT itemName FROM ShoppingList ORDER by itemName", null);
		cursor.moveToFirst();

		itemList = new ArrayList<String>();

		while (cursor.isAfterLast() == false) 
		{
			itemNames = cursor.getString(cursor.getColumnIndex("itemName"));
			itemList.add(itemNames);
			cursor.moveToNext();
		}

		cursor.close();
		myDbHelper.close();
		database.close();
	}


	/**
	 * Check for checked items and only save checked items and insert all
	 * checked items into the database
	 */
	private void checkedUncheckedItems() 
	{
		int count = itemListView.getAdapter().getCount();
		checkedPositions = itemListView.getCheckedItemPositions();
		for (int i = 0; i < count; i++) 
		{
			myDbHelper = new DataBaseHelper(this);
			database = myDbHelper.getWritableDatabase();
			myDbHelper.openDataBase();
			if (checkedPositions.get(i)) 
			{
				// CHECKED
				selectedItems = (String) (itemListView.getItemAtPosition(i));
				database.execSQL("DELETE FROM ShoppingList "
						+ " WHERE itemName = " + "'" + selectedItems + "';");
				Object toRemove = itemListView.getAdapter().getItem(i);
				adapter.remove((String) toRemove);
				database.close();
			}
		}

		adapter.notifyDataSetChanged();
	}


	/**
	 * Method populates store spinner
	 */
	public void populateStoreSpinner()
	{		
		final Spinner storeNameSpinner = (Spinner) findViewById(R.id.storeSpinner);	
		myDbHelper = new DataBaseHelper(ManageShoppingListActivity.this);
		myStore = new Store();

		ArrayList<String> arrayOfStoreNames = myStore.getListOfStores(myDbHelper);	
		ArrayAdapter<String> adapter = new ArrayAdapter<String>(ManageShoppingListActivity.this, R.layout.spinner_center_item, arrayOfStoreNames);
		storeNameSpinner.setAdapter(adapter);	

		storeNameSpinner.setOnItemSelectedListener(new OnItemSelectedListener() 
		{			
			public void onItemSelected(AdapterView<?> parentView, View selectedItemView, int position, long id) 
			{						
				String selectedStore = storeNameSpinner.getSelectedItem().toString();	
				storeName = selectedStore.substring(0, selectedStore.indexOf("(") - 1);		
				storeLocation = selectedStore.substring(selectedStore.indexOf("(") + 1, selectedStore.length()- 1);		
			}

			public void onNothingSelected(AdapterView<?> parentView){} 	
		});
	}	


	/**
	 * Method populates item names
	 */
	public void populateItemNames()
	{
		ListView shoppingItemsListView = (ListView) findViewById(R.id.shoppingItemsListView);

		ArrayList<String> arrayOfitemNames = mySortList.getItemNames(myDbHelper);	
		ArrayAdapter<String> adapter = new ArrayAdapter<String>(ManageShoppingListActivity.this, R.layout.spinner_center_item, arrayOfitemNames);
		shoppingItemsListView.setAdapter(adapter);	
	}


	/**
	 * Method handles deleting a shopping list
	 * @param v view
	 */
	public void deleteList(View v) 
	{
		myDbHelper = new DataBaseHelper(this);
		AlertDialog.Builder alert = new AlertDialog.Builder(
				ManageShoppingListActivity.this);
		alert.setTitle("Are you sure you want to delete your shopping list?");

		alert.setPositiveButton("Yes", new DialogInterface.OnClickListener() 
		{
			public void onClick(DialogInterface dialog, int whichButton) 
			{					
				myShoppingList.deleteList(myDbHelper);

				startActivity(new Intent(ManageShoppingListActivity.this,
						MainActivity.class));
			}
		});

		alert.setNegativeButton("Cancel",
				new DialogInterface.OnClickListener()
		{
			public void onClick(DialogInterface dialog, int whichButton) {}
		});

		alert.show();
	}

	/**
	 * Method handle adding items in a shopping list into a store
	 * @param v view
	 */
	public void addListToStore(View v) 
	{
		mySortList.addToStore(myDbHelper, storeName, storeLocation);
		myShoppingList.deleteList(myDbHelper);
		adapter.notifyDataSetChanged();

		Toast.makeText(getApplicationContext(),
				"Your shopping list has been added to " + storeName,
				Toast.LENGTH_SHORT).show();

		Intent refresh = new Intent(this, ManageShoppingListActivity.class);
		startActivity(refresh);
		this.finish();	
	}

	
	/**
	 * Method for creating a store Go to create a store activity upon button press	
	 * @param v view
	 */
	public void createStore(View v) 
	{
		startActivity(new Intent(ManageShoppingListActivity.this,
				CreateStoreActivity.class));
	}

	/**
	 * Method sorts shopping list based upon item locations
	 * @param v view
	 */
	public void sortList(View v) 
	{
		mySortList.sortShoppingList(myDbHelper, storeName, storeLocation);
		startActivity(new Intent(ManageShoppingListActivity.this,
				SortedItemsActivity.class));
	}

	
	/**
	 * Function that deletes the items selected in the list view
	 * @param v view
	 * @throws IOException
	 */
	public void deleteItem(View v) throws IOException
	{
		checkedUncheckedItems();
		Toast.makeText(getApplicationContext(),
				"Selected items have been deleted from your shopping list.",
				Toast.LENGTH_SHORT).show();

		ClearSelections();
		adapter.notifyDataSetChanged();

	}
}