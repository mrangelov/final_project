package com.example.shoppingcart;

/**
 * UI Class for creating shopping lists,
 * populating the main application 
 * database table and the shopping list table
 * @author Bertram Bishop
 * ITEC 4860 - Software Development Project
 */
import java.io.IOException;
import java.util.ArrayList;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.util.SparseBooleanArray;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnTouchListener;
import android.view.inputmethod.InputMethodManager;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.Toast;

public class CreateShoppingListActivity extends Activity 
{
	private String selectedItems;
	private ListView mainListView;
	private String itemNames;
	private MainList mainList;
	private DataBaseHelper myDbHelper;
	private SQLiteDatabase database;
	private ArrayList<String> itemList;
	private ArrayAdapter<String> listAdapter;
	final String SETTING_TODOLIST = "todolist";
	private SparseBooleanArray checkedPositions;
	protected int itemPosition;
	private ItemList myList;

	protected void onCreate(Bundle savedInstanceState) 
	{
		super.onCreate(savedInstanceState);
		setContentView(R.layout.create_shopping_list);
		mainListView = (ListView) findViewById(R.id.ItemslistView);

		createItemList();

		listAdapter = new ArrayAdapter<String>(this,
				android.R.layout.simple_list_item_multiple_choice, itemList);
		mainListView.setChoiceMode(ListView.CHOICE_MODE_MULTIPLE);
		mainListView.setAdapter(listAdapter);
		listAdapter.notifyDataSetChanged();
		
		RelativeLayout layout = (RelativeLayout) findViewById(R.id.layout);	
		layout.setOnTouchListener(new OnTouchListener()
		{
			@Override
			public boolean onTouch(View v, MotionEvent event) 
			{
				{
					hideKeyboard(v);
					return false;
				}
			}
		});	
	}

	/**
	 * Method enables keyboard to be hidden when clicked outside of editable text box
	 * @param view
	 */
	protected void hideKeyboard(View view)
	{
		InputMethodManager in = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
		in.hideSoftInputFromWindow(view.getWindowToken(), InputMethodManager.HIDE_NOT_ALWAYS);
	}
	

	/**
	 * Clear all selected checked items making them unchecked
	 */
	private void ClearSelections() 
	{
		int count = this.mainListView.getAdapter().getCount();

		for (int i = 0; i < count; i++) 
			this.mainListView.setItemChecked(i, false);		
	}


	/**
	 * Method displays messages if no items are selected and for items selected
	 */
	public void checkedUncheckedMessage() 
	{
		int msg = 0;
		if (mainListView.getCheckedItemPositions().size() == msg) 
		{
			Toast.makeText(getApplicationContext(),
					"No, Items saved to your list of items.",
					Toast.LENGTH_SHORT).show();
		}

		else 
		{
			listAdapter.notifyDataSetChanged();

			Toast.makeText(getApplicationContext(),
					"Items saved to your list of items.", Toast.LENGTH_SHORT)
					.show();

			Intent myIntent = new Intent(CreateShoppingListActivity.this,
					ManageShoppingListActivity.class);
			startActivity(myIntent);
		}
	}


	/**
	 * Check for checked items and only save checked items and insert all
	 * checked items into the database
	 */
	private void checkedUncheckedInsertItems() 
	{
		checkedPositions = mainListView.getCheckedItemPositions();
		int count = mainListView.getAdapter().getCount();

		for (int i = 0; i < count; i++) 
		{
			myDbHelper = new DataBaseHelper(this);
			database = myDbHelper.getWritableDatabase();
			myDbHelper.openDataBase();
			if (checkedPositions.get(i) == true) 
			{
				// CHECKED
				selectedItems = (String) (mainListView.getItemAtPosition(i));

				database.execSQL("INSERT INTO ShoppingList (itemName) VALUES("
						+ "'" + selectedItems + "'" + ");");
				database.execSQL("INSERT INTO SortList (itemName) VALUES("
						+ "'" + selectedItems + "'" + ");");
			}

			myDbHelper.close();
			database.close();
		}

		checkedUncheckedMessage();
	}


	/**
	 * Method checks for checked items and remove checked items only
	 */
	private void checkedUncheckedRemoveItems() 
	{
		int count = mainListView.getAdapter().getCount();
		checkedPositions = mainListView.getCheckedItemPositions();
		for (int j = 0; j < count; j++) 
		{
			myDbHelper = new DataBaseHelper(this);
			database = myDbHelper.getWritableDatabase();
			myDbHelper.openDataBase();
			if (checkedPositions.get(j))
			{
				// CHECKED
				selectedItems = (String) (mainListView.getItemAtPosition(j));
				database.execSQL("DELETE FROM MainShoppingList "
						+ " WHERE mainItemList = " + "'" + selectedItems + "';");
				Object toRemove = mainListView.getAdapter().getItem(j);
				listAdapter.remove((String) toRemove);
			}

			myDbHelper.close();
			database.close();
		}

		checkedPositions.clear();
		listAdapter.notifyDataSetChanged();
		Toast.makeText(getApplicationContext(),
				"Selected items have been deleted from your list.",
				Toast.LENGTH_SHORT).show();
	}


	/**
	 * Function that traverses and find all items in the main table for the
	 * listview to display
	 */
	public void createItemList() 
	{
		myDbHelper = new DataBaseHelper(this);
		database = myDbHelper.getWritableDatabase();
		myDbHelper.openDataBase();
		Cursor cursor = database
				.rawQuery(
						"SELECT mainItemList FROM MainShoppingList ORDER by mainItemList",
						null);
		cursor.moveToFirst();

		itemList = new ArrayList<String>();

		while (cursor.isAfterLast() == false) 
		{
			itemNames = cursor.getString(cursor.getColumnIndex("mainItemList"));
			itemList.add(itemNames);
			cursor.moveToNext();
		}
		
		cursor.close();
		myDbHelper.close();
		database.close();

	}

	
	/**
	 * Function adds new items to the database by user adding items in the add
	 * items text field and saving to the main table. Also check for duplicates
	 * in the table
	 * @param v view
	 * @throws IOException
	 */
	public void addNewItem(View v) throws IOException 
	{

		mainList = new MainList();
		DataBaseHelper myDbHelper = new DataBaseHelper(this);
		EditText newItemText = (EditText) findViewById(R.id.newItemTextV);
		String itemNames = newItemText.getText().toString().trim();

		if (isEmpty(newItemText) == true)
			newItemText.setError("Please enter an item name");

		else if (mainList.hasDuplicateItems(myDbHelper, itemNames) == true) 
		{
			new AlertDialog.Builder(this)
			.setTitle("Error")
			.setMessage(
					"The item you entered already exists. Please enter a new item name")
					.setPositiveButton("Ok",
							new DialogInterface.OnClickListener() 
					{
						public void onClick(DialogInterface dialog,
								int which) 	{}
					}).show();
			Toast.makeText(getApplicationContext(),
					" Please enter another item.", Toast.LENGTH_SHORT).show();
		}

		else if (mainList.hasDuplicateItems(myDbHelper, itemNames) == false) 
		{
			// Adds new item to the top of the list
			itemList.add(0, newItemText.getText().toString());
			listAdapter.notifyDataSetChanged();
			newItemText.setText("");

			// Adds list to database
			MainList mainList = new MainList();
			mainList.addList(myDbHelper, itemNames.trim());

			myDbHelper.close();
			database.close();

			// Resets fields
			newItemText.setText("");
		}
	}
	

	/**
	 * Method deletes items from the database and remove them from the list view
	 * @param v view
	 * @throws IOException
	 */
	public void removeItem(View v) throws IOException 
	{
		checkedUncheckedRemoveItems();
		Toast.makeText(getApplicationContext(),
				"Selected items have been deleted.", Toast.LENGTH_SHORT).show();
		ClearSelections();
	}
	

	/**
	 * Function that saves the items selected in the list view and saves them in
	 * the shopping list table. Also checks for duplicate items
	 * @param v
	 * @throws IOException
	 */
	public void saveList(View v) throws IOException 
	{
		myList = new ItemList();

		checkedPositions = mainListView.getCheckedItemPositions();
		int count = mainListView.getAdapter().getCount();
		for (int i = 0; i < count; i++) 
		{
			if (checkedPositions.get(i) == true)
			{
				// CHECKED
				selectedItems = (String) (mainListView.getItemAtPosition(i));
			}
		}
		
		if (myList.hasDuplicateItems(myDbHelper, selectedItems) == true) 
		{
			new AlertDialog.Builder(this)
			.setTitle("Error")
			.setMessage(
					"The item/items you've checked already exists. Please check a different item")
					.setPositiveButton("Ok",
							new DialogInterface.OnClickListener() 
					{
						public void onClick(DialogInterface dialog,
								int which) {}
					}).show();
			ClearSelections();
		}

		else if (myList.hasDuplicateItems(myDbHelper, selectedItems) == false) 
		{
			checkedUncheckedInsertItems();
			ClearSelections();
		}
	}

	
	/**
	 * Function for checking if text field is empty
	 * @param etText
	 * @return whether or not a textfield is empty
	 */
	private boolean isEmpty(EditText etText) 
	{
		if (etText.getText().toString().trim().length() > 0) 
			return false;
		
		else 
			return true;
	}
}